// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyBIcOpTL2Pt-0hNACLMFutR2RFZ2QJo5Ew",
  clientId: "788909569445-mcb90hu89ehnpqeammjucobjna4v0kiu.apps.googleusercontent.com",
  discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest", "https://content.googleapis.com/discovery/v1/apis/admin/directory_v1/rest"],
  scope: ["https://www.googleapis.com/auth/calendar", "https://www.googleapis.com/auth/admin.directory.resource.calendar.readonly", "https://www.googleapis.com/auth/admin.directory.resource.calendar"].join(' '),
  calendarId: "vadim.sidorchuk@logicify.com", 
  room: "logicify.com_3833343838303031393438@resource.calendar.google.com",
  roomName: 'Select room'
};

export const config = {
  timeMeetingStartSoon: 15,
  nameNewEvent: "Ad hoc meeting",
  thereNoEvents: "No events",
  availableTime: 90,
  idleTime: 60 * 1000
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
