// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environmentFelix = {
  production: false,
  apiKey: "AIzaSyA8b_AU-HPWy02wCRsgRb4mIL2bzFVM-wE",
  clientId: "722714492512-hduvlhnbm57gei6gham6klhhqui5eqs0.apps.googleusercontent.com",
  discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
  scope:"https://www.googleapis.com/auth/calendar.readonly",
  calendarId: "felya.van@gmail.com"  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
