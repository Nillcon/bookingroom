import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GapiAuthClient } from './gapi-auth-client';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GapiCalendarClient {

  private readonly customer: string = "my_customer";

  constructor(private authClient: GapiAuthClient) {}

  public getCalendarsList(): Observable<any> {
    return this.authClient.isClientAuth()
      .pipe(
        switchMap(() => gapi.client['calendar'].calendarList.list({ }))
      )
  }

  public getRoomList(customer: string = this.customer): Observable<any> {
    return this.authClient.isClientAuth()
    .pipe(
      switchMap(() => gapi.client['directory'].resources.calendars.list({
        "customer": customer
      }))
    )
  }

  public getEventsList(calendarId: string, timeMin: string, timeMax: string, orderBy: string, singleEvents: boolean): Observable<any> {
    return this.authClient.isClientAuth()
      .pipe(
        switchMap(() => gapi.client['calendar'].events.list({
          calendarId: calendarId,
          timeMin: timeMin,
          timeMax: timeMax,
          orderBy: orderBy,
          singleEvents: singleEvents
        }))
      )
  }

  public insertEvent(calendarId: string, start: Object, end: Object, summary: string, attendees: Array<any>): Observable<any> {
    return this.authClient.isClientAuth()
      .pipe(
        switchMap(() => gapi.client['calendar'].events.insert({
          calendarId: calendarId,
          summary: summary,
          start: start,
          end: end,
          attendees: attendees
        }))
      ) 
  }

  public getEvent(calendarId, eventId): Observable<any> {
    return this.authClient.isClientAuth()
    .pipe(
      switchMap(() => gapi.client['calendar'].events.get({
        calendarId: calendarId,
        eventId: eventId
      }))
    )
  }
}