import { Injectable } from '@angular/core';
import { GoogleApiService } from 'ng-gapi';
import { mergeMap, switchMap, map, tap, filter } from 'rxjs/operators';
import { Observable, ReplaySubject, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { fromPromise } from 'rxjs/internal-compatibility';

@Injectable({
  providedIn: 'root'
})
export class GapiAuthClient {

  constructor(
    private googleApi: GoogleApiService
  ) { }

  private _isAuth: ReplaySubject<boolean> = new ReplaySubject(1);

  public loadAuthClient(): Observable<any>{
    return this.googleApi.onLoad()
      .pipe(
        mergeMap(() => Observable.create((observer) => this.initAuthClient(observer))),
        switchMap(() => this.initClient()),
        map(() => this._isAuth.next(gapi.auth2.getAuthInstance().isSignedIn.get()))
      )   
  }

  private initAuthClient(observer) {
    gapi.load('auth2', () => {
      gapi.auth2.init(this.googleApi.getConfig().getClientConfig())
        .then((auth) => {
          observer.next(auth);
          observer.complete();
        },
        error => {
          return throwError(error);
        }
      );
    })
  }

  private initClient(): Observable<any> {
    return new Observable((subscriber) => {
      gapi.load('client', () => {
        subscriber.next();
        subscriber.complete();
      })
    }).pipe(
        switchMap(() => gapi.client.init({
          apiKey: environment['apiKey'],
          clientId: environment['clientId'],
          discoveryDocs: environment['discoveryDocs'],
          scope: environment['scope']
        }))
    );
  }

  public SignedIn(): Observable<any> {
    let auth2 = gapi.auth2.getAuthInstance();
    
    return fromPromise(auth2.signIn())
      .pipe(
        tap(() => {
          this._isAuth.next(gapi.auth2.getAuthInstance().isSignedIn.get());
        })
      )
  }

  public SignedOut(){
    let auth2 = gapi.auth2.getAuthInstance();

    this._isAuth.next(false)
    localStorage.clear();

    return fromPromise(auth2.signOut())
  }

  public get isAuth(): Observable<any> {
    return this._isAuth.asObservable();
  }

  public isClientAuth(): Observable<any> {
    return this._isAuth.asObservable()
      .pipe(
        filter((value) => value === true)
      )
  }
}