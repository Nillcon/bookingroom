import { Component, OnInit, Input } from '@angular/core';
import { timer } from 'rxjs';
import { DateMethod } from 'src/app/Methods/date.method';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from './../base/base.component';

@Component({
  selector: 'app-timescale',
  templateUrl: './timescale.component.html',
  styleUrls: ['./timescale.component.scss']
})
export class TimescaleComponent extends BaseComponent implements OnInit {
  @Input('timescale') timescale: Array<any>;
  @Input('events') events: Array<any>;
  @Input('bookingEvent') bookingEvent: {};
  @Input('scrollOffset') scrollOffset: number;

  public currentTime: Number;
  
  constructor(
    private _scrollToService: ScrollToService
  ) {
    super();
  }

  ngOnInit() {
    this.updateCurrentTime();
  }

  public updateCurrentTime() {
    let interval = timer(0, 1000);

    interval
      .pipe(
        takeUntil(this.componetDestroyed)
      )
      .subscribe(() => {
        this.currentTime = DateMethod.minutesToPixels(DateMethod.getMinutes(new Date()));
      })
  }

  public triggerScrollTo() {
    
    const config: ScrollToConfigOptions = {
      target: 'destinationRef',
      container: 'wrapper'
    };

    this._scrollToService.scrollTo(config);
  }
}
