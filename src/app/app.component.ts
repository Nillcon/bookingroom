import { Component, OnInit, ViewEncapsulation, NgZone, OnDestroy } from '@angular/core';
import { AppService } from './app.component.service';
import { EventService } from './Services/event.service';
import { takeUntil, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from './Services/auth.service';
import { ErrorService } from './Services/error.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [AppService],
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AppComponent implements OnInit, OnDestroy {


  constructor(
    public appService: AppService,
    public eventService: EventService,
    private router: Router,
    private zone: NgZone,
    private authService: AuthService,
    public errorService: ErrorService
  ) {}

  private componetDestroyed: Subject<any> = new Subject();

  ngOnInit() {
    this.authService.loadAuthClient()
      .pipe(
        takeUntil(this.componetDestroyed),
        switchMap(() => this.authService.isAuth)
      )
      .subscribe(
        (result) => {
          if (result === true) {
            this.eventService.updateEvents();
          } else if (result === false) {
            this.zone.run(() => {
              this.router.navigate(['signIn']);
            })
          }
        },
        (error) => {
          alert(error);
          console.log(error)
        }
      );
  }

  ngOnDestroy() {
    this.componetDestroyed.next();
    this.componetDestroyed.unsubscribe();
  }
}
