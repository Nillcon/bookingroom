import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { environment } from './../environments/environment';

import { GoogleApiModule, NgGapiClientConfig, NG_GAPI_CONFIG } from "ng-gapi";

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { AppComponent } from './app.component';
import { IndexComponent } from './Pages/index/index.component';
import { EventComponent } from './Pages/event/event.component';
import { PageNotFoundComponent } from './Pages/page-not-found/page-not-found.component';
import { LoaderComponent } from './Components/loader/loader.component';
import { SettingComponent } from './Pages/setting/setting.component';
import { SignInComponent } from './Pages/sign-in/sign-in.component';

import { GuardPage } from './Guard/guard-page.service';
import { GuardSignIn } from './Guard/guard-sign-in.service';
import { GuardSetting } from './Guard/guard-setting.service';

import { ForInObjectPipe } from './shared/for-in-object.pipe';
import { InPipe } from './shared/in.pipe';
import { TimescaleComponent } from './Components/timescale/timescale.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { 
  MatButtonModule, 
  MatInputModule, 
  MatIconModule, 
  MatSelectModule, 
  MatMenuModule,
  MatFormFieldModule,
  MatDialogModule,
  MatSnackBarModule
} from '@angular/material';

let gapiClientConfig: NgGapiClientConfig = {
  client_id: environment.clientId,
  discoveryDocs: environment.discoveryDocs,
  scope: environment.scope
};

const routes: Routes = [
  { path: '', component: IndexComponent, canActivate: [GuardPage] },
  { path: 'index', redirectTo: '', canActivate: [GuardPage] },
  { path: 'event', component: EventComponent, canActivate: [GuardPage] },
  { path: 'setting', component:  SettingComponent, canActivate: [GuardSetting]},
  { path: 'signIn', component: SignInComponent, canActivate: [GuardSignIn] },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    EventComponent,
    PageNotFoundComponent,
    LoaderComponent,
    SettingComponent,

    ForInObjectPipe,
    InPipe,
    TimescaleComponent,
    SignInComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
    ScrollToModule.forRoot(),

    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatMenuModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSnackBarModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent],
  exports: [ 
    RouterModule,
    MatButtonModule
  ],
  entryComponents:[]
})
export class AppModule {}
