export module DateMethod {

    export function getMinDate() {
        let date = new Date();
        date.setHours(0, 0, 0, 0);
        return date;
    }

    export function getMaxDate() {
        let date = new Date();
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999);
    }

    export function diffDate(date1: Date, date2: Date) {
        let result = getMinDate();
        result.setMilliseconds(Math.abs(date1.getTime() - date2.getTime()));

        return result;
    }

    export function timeToPixels(date: Date) {
        let minutes = getMinutes(date);
        let result = Math.floor(minutes * (51 / 15) - 5);
        return result;
    }
    export function minutesToPixels(minutes: number) {
        return Math.floor(minutes * (51 / 15) - 5);
    }

    export function getMinutes(date: Date) {
        let minDate = getMaxDate();

        return (((date.getDate()  - minDate.getDate()) * 24 * 60)) + (date.getHours() * 60) + date.getMinutes();
    }

    export function getRuler() {
        let result: Array<any> = [];
        for (let i = 0; i <= 23; i++) {
          for (let j = 0; j <= 45; j += 15) {
            result.push({
              hours: (i < 10)? `0${i}` : `${i}`,
              minutes: (j < 10)? `0${j}` : `${j}`
            })
          }
        }

        return result;
    }
}