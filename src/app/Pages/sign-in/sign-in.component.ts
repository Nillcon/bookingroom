import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { ErrorService } from 'src/app/Services/error.service';
import { BaseComponent } from 'src/app/Components/base/base.component';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./../../../assets/css/modal.window.scss']
})
export class SignInComponent extends BaseComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private zone: NgZone,
    private router: Router,
    private errorService: ErrorService
  ) { 
    super();
  }

  ngOnInit() {
  }

  public SingIn() {
    this.authService.SignedIn()
      .pipe(
        takeUntil(this.componetDestroyed)
      )
      .subscribe(
        () => this.zone.run(() => this.router.navigate(['setting'])),
        error => {
          this.errorService.alertGapiAuthError(error);
          this.zone.run(() => this.router.navigate(['setting']));
        }
      )
  }

}
