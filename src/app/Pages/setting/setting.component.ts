import { Component, OnInit, NgZone } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';
import { ErrorService } from 'src/app/Services/error.service';
import { SettingService } from 'src/app/Services/setting.service';
import { BaseComponent } from './../../Components/base/base.component';
import { CalendarInfo } from 'src/app/Interfaces/calendarInfo';
import { RoomInfo } from 'src/app/Interfaces/roomInfo';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./../../../assets/css/modal.window.scss']
})
export class SettingComponent extends BaseComponent implements OnInit {

  constructor(
    private zone: NgZone,
    private router: Router,
    private errorService: ErrorService,
    private settingService: SettingService,
    private authService: AuthService
  ) { 
    super();
  }
  public settingIsExist: boolean;

  public calendarList: Array<any>;
  public currentCalendar: CalendarInfo;

  public roomList: Array<any>;
  public currentRoom: RoomInfo;

  ngOnInit() {
    this.settingService.getRoomList()
    .pipe(
      takeUntil(this.componetDestroyed)
    )
    .subscribe((result) => this.zone.run(() => this.roomList = result),
      error => {
        this.errorService.alertGapiError(error);
      });

    this.settingService.getCalendarsList()
    .pipe(
      takeUntil(this.componetDestroyed)
    )
    .subscribe(result => this.zone.run(() => this.calendarList = result),
      error => {
        this.errorService.alertGapiError(error);
      });
    
    if (this.settingService.settingIsExist()) {
      this.settingIsExist = true;
      this.currentCalendar = this.settingService.getCalendarInfo();
      this.currentRoom = this.settingService.getRoomInfo();
    } else {
      this.settingIsExist = false;
    }
  }

  public saveRoom() {
    if (this.currentCalendar && this.currentRoom) {
      this.settingService.setSetting(this.currentRoom, this.currentCalendar);
    } 
    
    window.location.href = '/';    
  }

  public cansel() {
    this.router.navigate(['/']);
  }

  public exit() {
    this.authService.SignedOut();
  }
}
