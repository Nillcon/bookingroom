export const BookButtons: Array<any> = [
    {
      minutes: 15,
      text: "15m"
    },
    {
      minutes: 30,
      text: "30m"
    },
    {
      minutes: 45,
      text: "45m"
    },
    {
      minutes: 60,
      text: "1hm"
    },
    {
      minutes: 90,
      text: "1h 30m"
    }
]