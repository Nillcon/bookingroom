import { Component, OnInit, NgZone } from '@angular/core';
import { EventService } from '../../Services/event.service';
import { Router } from '@angular/router';
import { DateMethod } from './../../Methods/date.method';
import { CurrentStatus } from 'src/app/Interfaces/currentStatus';
import { BookButtons } from './events.config';
import { EVENT_STATE_TEXT, CurrentRoomState } from '../../state-names';
import { config } from './../../../environments/environment';
import { Observable, Subscription, merge, of, fromEvent, interval } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import { ErrorService } from './../../Services/error.service';
import { NotifyService } from './../../Services/notify.service';
import { BaseComponent } from 'src/app/Components/base/base.component';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent extends BaseComponent implements OnInit {

  public currentState: CurrentStatus = null;
  public blockEvents: Array<any> = null;
  public buttonText: string;
  public bookingEvent: Object;
  public bookingInProcess: boolean = false;
  public timeRuler: Array<any> = [];
  public availableBlock: Object;

  public scrollOffset;

  public BookButtons = BookButtons;
  public CurrentRoomState = CurrentRoomState;

  public idle$: Observable<any>;
  public time: Subscription;

  constructor(
    private eventService: EventService,
    private zone: NgZone,
    private router: Router,
    private errorService: ErrorService,
    private notify: NotifyService
  ) { 
    super();
  }

  ngOnInit() {
    this.getRuler();

    this.idleEvents();

    this.scrollOffset = DateMethod.timeToPixels(new Date());
    this.updateCurrentStatus();
    this.updateAvailableBlock();
    this.updateBlockEvents();
  }

  public updateCurrentStatus() {
    this.eventService.currentState$
    .pipe(
      takeUntil(this.componetDestroyed)
    )
    .subscribe(response => this.zone.run(() => {
      if (response) {
        const texts = EVENT_STATE_TEXT[response.status];
        this.currentState = response;
        this.buttonText = texts.buttonText;
      }
    }));
  }

  public updateBlockEvents() {
    this.eventService.getBlockEvents$
    .pipe(
      takeUntil(this.componetDestroyed)
    )
    .subscribe(response => this.zone.run(() => {
      this.blockEvents = response;
    }));
  }

  public updateAvailableBlock() {
    this.eventService.getAvailableBlock$
    .pipe(
      takeUntil(this.componetDestroyed)
    )
    .subscribe(response => this.zone.run(() => {
      this.availableBlock = response;
    }))
  }

  public getRuler() {
    //generate timescale
    this.timeRuler = DateMethod.getRuler();
  }

  public swipe() {
    this.router.navigateByUrl('/');
  }

  public bookEvent(index) {
    //Write down data on the reserved time
    let result = {
      height: DateMethod.minutesToPixels(this.BookButtons[index]['minutes']),
      minutes: this.BookButtons[index]['minutes'],
      offset: this.availableBlock['offset'],
      start: this.availableBlock['start']
    }

    let endDate = new Date(this.availableBlock['start']);
    endDate.setMinutes(endDate.getMinutes() + this.BookButtons[index]['minutes']);

    result['end'] = endDate;

    this.bookingEvent = result;
    this.scrollOffset = result['offset'] + Math.random();
  }

  public cancelBooking() {
    //Clear bookingEvent
    this.bookingEvent = null;
    let minutes = DateMethod.minutesToPixels(DateMethod.getMinutes(new Date()))
    this.scrollOffset = minutes + Math.random();
  }

  public createEvent() {
    this.bookingInProcess = true;
    this.eventService.insertEvent(this.availableBlock['start'], this.bookingEvent['end'])
    .pipe(
      takeUntil(this.componetDestroyed),
      switchMap(() => this.eventService.getEvents())
    )
    .subscribe(() => this.zone.run(() => {
      this.notify.show("Event added!", { duration: 3000 });

      this.bookingInProcess = false;
      this.cancelBooking();
    }),
    error => this.errorService.alertGapiError(error)) 
  }
  
  public idleEvents() {
    merge(of(null), fromEvent(window, 'touchend'))
      .pipe(
        switchMap(() => interval(config.idleTime)),
        takeUntil(this.componetDestroyed)
      )
      .subscribe((n) => {
        this.router.navigate(['/'])
      });
  }
}