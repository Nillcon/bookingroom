import { Component, OnInit, NgZone } from '@angular/core';
import { EventService } from './../../Services/event.service';
import { CurrentStatus } from './../../Interfaces/currentStatus';
import { Router } from '@angular/router';
import { EVENT_STATE_TEXT, CurrentRoomState } from '../../state-names';
import { config } from './../../../environments/environment';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/Services/auth.service';
import { ErrorService } from 'src/app/Services/error.service';
import { BaseComponent } from './../../Components/base/base.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent extends BaseComponent implements OnInit {

  constructor(
    private eventService: EventService,
    private zone: NgZone,
    private router: Router,
    private authService: AuthService,
    private errorService: ErrorService
  ) {
    super();
  }

  public currentState: CurrentStatus = null;

  public room: string = 'Hall Room';
  public availibleText: string;
  public thereNoEvents: string = config.thereNoEvents;
  public buttonText: string;

  public loadCalendars;

  public CurrentRoomState = CurrentRoomState;

  public eventStateSubscribe = null;
  
  ngOnInit() {
    this.updateCurrentStatus();    
  }

  public updateCurrentStatus() {
    this.eventStateSubscribe = this.eventService.currentState$
      .pipe(
        takeUntil(this.componetDestroyed)
      )
      .subscribe(
        response => this.zone.run(() => {
          if (response) {
            this.currentState = response;
            const texts = EVENT_STATE_TEXT[response.status];
            this.availibleText = texts.statusText;
            this.buttonText = texts.buttonText;
          }
        },
        error => this.errorService.alertGapiError(error)
      ));
  }

  public swipe() {
    this.router.navigateByUrl('/event');
  }

  public changeRoom() {
    this.zone.run(() => {
      this.router.navigate(['setting']);
    })
  }

  public SignedOut() {
    this.authService.SignedOut();
  }

  public fullScreen() {
    document.documentElement['webkitRequestFullScreen']()
  }
}
