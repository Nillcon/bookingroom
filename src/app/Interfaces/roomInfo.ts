export interface RoomInfo {
    kind?: string;
    etags?: string,
    resourceId?: string,
    resourceName?: string,
    generatedResourceName?: string,
    resourceEmail?: string,
    capacity?: number,
    buildingId?: string,
    floorName?: string,
    resourceCategory?: string,
    userVisibleDescription?: string,
    featureInstances?: Array<any>
}