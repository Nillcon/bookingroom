export interface CalendarInfo {
    kind: string,
    etag: string,
    id: string,
    summary: string,
    timeZone: string,
    colorId: string,
    backgroundColor: string,
    foregroundColor: string,
    selected: boolean,
    accessRole: string,
    defaultReminders: Array<any>,
    notificationSettings: Object,
    primary: boolean,
    conferenceProperties: Object
}