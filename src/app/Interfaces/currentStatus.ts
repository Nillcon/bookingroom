import { CurrentRoomState } from "../state-names";

export interface CurrentStatus {
    status?: CurrentRoomState;
    time?: Date;
}