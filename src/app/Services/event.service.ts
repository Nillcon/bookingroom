import { Injectable } from '@angular/core';
import { DateMethod } from './../Methods/date.method';
import { config } from './../../environments/environment';
import { timer, BehaviorSubject, ReplaySubject, Observable, of, throwError } from 'rxjs';
import { CurrentStatus } from './../Interfaces/currentStatus';
import { first, switchMap, tap, delay, map, retry, mergeMap, catchError, filter } from 'rxjs/operators';
import { CurrentRoomState } from '../state-names';
import { ErrorService } from './error.service';
import { AuthService } from './auth.service';
import { GapiCalendarClient } from './../Gapi-client/gapi-calendar-client';
import { SettingService } from './setting.service';
import { RoomInfo } from '../Interfaces/roomInfo';
import { CalendarInfo } from '../Interfaces/calendarInfo';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(
    public errorService: ErrorService,
    public authService: AuthService,
    private gapiCalendarClient: GapiCalendarClient,
    private settingService: SettingService
  ) { }
  private currentEvent: Object;
  private currentState: CurrentStatus;

  private events: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
  private _currentState$: ReplaySubject<CurrentStatus> = new ReplaySubject();
  private blockEvents$: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
  private availableBlock$: ReplaySubject<Object> = new ReplaySubject();

  public updateEvents() {
    const delay = timer(0, 30000);
    //Update events every 30 seconds
    delay
      .pipe(
        switchMap(() => this.getEvents()),
        catchError(error => {
          this.errorService.alertGapiError(error);

          return of(false);
        })
      )
      .subscribe()
      
    //After first getting event getting started update current state
    this.events
      .pipe(
        first((value) => value != null),
        map(() => this.updateCurrentState())
      ).subscribe()
  }

  public getEvents(): Observable<any> {
    if (this.settingService.settingIsExist()) {
      let roomInfo: RoomInfo =  this.settingService.getRoomInfo();

      return this.gapiCalendarClient.getEventsList(
        roomInfo.resourceEmail, 
        DateMethod.getMinDate().toISOString(), 
        DateMethod.getMaxDate().toISOString(), 
        "startTime", 
        true
      )
        .pipe(
          map((response) => response['result']['items']),
          tap(events => {
            this.events.next(events);
            this.currentEvent = this.getCurrentEvent(events);
  
            this.updateEventsBlock(events);
          })
        )

    } else {
      return of(false);
    }
  }

  public updateCurrentState() {
    const delay = timer(0, 1000);

    //******
    delay.subscribe(() => {
      let result: CurrentStatus;

      if (this.currentEvent) {
        let currStateDateStart = new Date(this.currentEvent['start']['dateTime']);
        let currStateDateEnd = new Date(this.currentEvent['end']['dateTime']);
        let startTime = new Date();
        
        if (currStateDateEnd.getTime() > startTime.getTime()){
          if (currStateDateStart.getTime() > startTime.getTime()) {  
            let time = DateMethod.diffDate(startTime, currStateDateStart);
            if (DateMethod.getMinutes(time) >= config.timeMeetingStartSoon) {
              result = { status: CurrentRoomState.AVAILABLE };
            } else {
              result = { status: CurrentRoomState.MEETING_SOON };
            }
            result.time = time;
          } else {
            result = {
              time: DateMethod.diffDate(startTime, currStateDateEnd),
              status: CurrentRoomState.IN_PROGRESS
            };
          }
        } else {
          this.getEvents().subscribe()
        }
      } else {
        result = { status: CurrentRoomState.AVAILABLE };
      }
      this._currentState$.next(result);
      this.currentState = result;
      if (result) {
        this.searchFreeTime();
      }
    })
  }

  public updateEventsBlock(events) {
    //Update block array with events
    let result: Array<any> = [];

    for (let i = 0; i < events.length; i++) {

      let dateStart = new Date(events[i]['start']['dateTime']);
      let dateEnd =  new Date(events[i]['end']['dateTime'])

      //search event duration
      let diffDate = DateMethod.diffDate(dateStart, dateEnd);
      //search offset
      let offsetDiffTime = DateMethod.diffDate(DateMethod.getMinDate(), dateStart);

      result.push({
        start: dateStart,
        end: dateEnd,
        height: DateMethod.timeToPixels(diffDate),
        name: events[i]['summary'],
        offset: DateMethod.timeToPixels(offsetDiffTime)
      })
    }
    this.blockEvents$.next(result);
  }

  private getCurrentEvent(events) {
    //Search next event
    if (events.length) {
      for (let i = 0; i < events.length; i++) {
        if (new Date(events[i]['end']['dateTime']).getTime() > new Date().getTime()) {
          return events[i];
        }
      }
    }
  }

  public searchFreeTime(){
    let result = {}
    let currentState = this.currentState;
    let blockEvents = this.blockEvents$.getValue();
    if (blockEvents) {
      if (blockEvents.length)  {
        for (let i = 0; i < blockEvents.length; i++) {
          if (new Date().getTime() < blockEvents[i]['end'].getTime()) {
            let start: Date;
            let end: Date;

            start = (currentState.status === CurrentRoomState.AVAILABLE)? new Date(): blockEvents[i]['end'];

            if (currentState.status === CurrentRoomState.AVAILABLE) {
              start = new Date();
              end = blockEvents[i]['start'];
            } else {
              start = blockEvents[i]['end'];
              if (blockEvents[i + 1]) {
                end = blockEvents[i + 1]['start'];
              } else {
                this.setAvailableBlock(start);
                return;
              }
            }
            let diffDate = DateMethod.diffDate(start, end);
            result['minutes'] = DateMethod.getMinutes(diffDate);

            if (result['minutes'] > 14) {
              this.setAvailableBlock(start, result['minutes']);
              return;
            } else {
              continue;
            }
          }
        }
        this.setAvailableBlock();
      } else {
        this.setAvailableBlock();
      }
    }
  }

  public setAvailableBlock(start = new Date(), minutes = 90) {
    let result = {
      start: start,
      minutes: minutes,
      offset: DateMethod.minutesToPixels(DateMethod.getMinutes(start))
    }
    this.availableBlock$.next(result)
  }

  public insertEvent (start: Date, end: Date): Observable<any> {
    if (this.settingService.settingIsExist()) {
      let calendarInfo: CalendarInfo = this.settingService.getCalendarInfo();
      let roomInfo: RoomInfo = this.settingService.getRoomInfo();

      return this.gapiCalendarClient.insertEvent(
        calendarInfo.id,
        { dateTime: start.toISOString() },
        { dateTime: end.toISOString() },
        config.nameNewEvent,
        [{ email: roomInfo.resourceEmail }]
      )
        .pipe(
          map(response => response['result']),
          switchMap((event) => this.waitForRoomResponse(event)),
          tap((event) => this.events.next([].concat(this.events.value, event))),
          switchMap(() => this.getEvents())
        )        
    } else {
      return of(false);
    }    
  }

  private waitForRoomResponse(event): Observable<any> {
    let calendarInfo: CalendarInfo = this.settingService.getCalendarInfo();
    let roomInfo: RoomInfo = this.settingService.getRoomInfo();

    return of(null)
      .pipe(
        delay(300),
        switchMap(() => this.gapiCalendarClient.getEvent(calendarInfo.id, event.id)),
        mergeMap(response => response['result']['attendees']),
        filter(attendee => attendee['email'] == roomInfo.resourceEmail),
        mergeMap(result => {
          if (result['responseStatus'] != 'needsAction') {
            return of(result);
          } else {
            return throwError('Error')
          }
        }),
        retry(20)
      );
  }

  public get currentState$() {
    return this._currentState$.asObservable();
  }

  public get getAvailableBlock$() {
    return this.availableBlock$.asObservable();
  }

  public get getBlockEvents$() {
    return this.blockEvents$.asObservable();
  }
} 