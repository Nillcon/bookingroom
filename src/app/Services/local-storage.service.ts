import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  public get(property: string) {
    return JSON.parse(localStorage.getItem(property));
  }

  public set(property: string, value: any) {
    localStorage.setItem(property, JSON.stringify(value));
  }
}
