import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  public show(message: string, option?: {}, action?: string) {
    this.snackBar.open(
      message, 
      action || 'Undo',
      option || {}
    );
  }
}
