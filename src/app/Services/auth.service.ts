import { Injectable } from '@angular/core';
import { GapiAuthClient } from '../Gapi-client/gapi-auth-client';
import { Observable } from 'rxjs';
import { ErrorService } from './error.service'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private gapiAuth: GapiAuthClient,
    private errorService: ErrorService,
  ) { }

  public loadAuthClient(): Observable<any> {
    return this.gapiAuth.loadAuthClient()
  }

  public get isAuth() {
    return this.gapiAuth.isAuth;
  }

  public SignedIn(): Observable<any> {
    return this.gapiAuth.SignedIn();
  }

  public SignedOut() {
    this.gapiAuth.SignedOut();
  }
}
