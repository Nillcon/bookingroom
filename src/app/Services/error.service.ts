import { Injectable, NgZone } from '@angular/core';
import { ERROR_MESSAGE, ErrorName } from 'src/app/error-names';
import { NotifyService } from './notify.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private notify: NotifyService,
    private zone: NgZone,
    private router: Router
  ) { }

  public alertGapiAuthError(error) {
    this.zone.run(() => {
      this.notify.show(error.details || ERROR_MESSAGE[ErrorName[error.error]]);
    });
  }

  public alertGapiError(error) {
    this.zone.run(() => {
      let resultError = error.result.error;

      if (resultError) {
        this.zone.run(() => this.notify.show(`Code: ${resultError.code} \nMessage: ${resultError.message}`))

        if (resultError.code == 404) {
          this.zone.run(() =>this.router.navigate(['setting']));
        }
      } else {
        return false;
      }
    })
  }
}
