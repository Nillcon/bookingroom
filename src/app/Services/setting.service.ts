import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GapiCalendarClient } from '../Gapi-client/gapi-calendar-client';
import { map } from 'rxjs/operators';
import { LocalStorageService } from './local-storage.service';
import { RoomInfo } from './../Interfaces/roomInfo';
import { CalendarInfo } from './../Interfaces/calendarInfo';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  private readonly room: string = 'roomInfo';
  private readonly calendar: string = 'calendarInfo';

  constructor(
    private gapiCalendarClient: GapiCalendarClient,
    private localStorageService: LocalStorageService
  ) { }

  public getRoomList(): Observable<any> {
    return this.gapiCalendarClient.getRoomList()
      .pipe(
        map((response) => response['result']['items'])
      )
  }

  public getCalendarsList(): Observable<any> {
    return this.gapiCalendarClient.getCalendarsList()
      .pipe(
        map((response) => response['result']['items'])
      )
  }

  public getRoomInfo(): RoomInfo {
    return this.localStorageService.get(this.room);
  }

  public getCalendarInfo(): CalendarInfo {
    return this.localStorageService.get(this.calendar);
  }

  public setSetting(roomInfo: RoomInfo, calendarInfo: CalendarInfo) {
    this.localStorageService.set(this.room, roomInfo);
    this.localStorageService.set(this.calendar, calendarInfo);
  }

  public settingIsExist(): boolean {
    if (this.getRoomInfo() && this.getCalendarInfo()) {
      return true;
    } else {
      return false;
    }
  }
}
