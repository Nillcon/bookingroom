import { Injectable } from '@angular/core';
import { EventService } from './Services/event.service';

@Injectable({
  providedIn: 'root'
})
export class AppService{

  constructor(public eventService: EventService) { }
}
