import { Injectable, NgZone } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './../Services/auth.service';
import { tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class GuardSetting implements CanActivate {

  constructor(
    public authService: AuthService,
    private zone: NgZone,
    private router: Router
  ) {}

  canActivate() {
    return this.authService.isAuth
      .pipe(
        tap(result => {
          if (result === false) {
            this.zone.run(() => this.router.navigate(['signIn']))
          }
        })
      )
  }
}
