import { Injectable, NgZone } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './../Services/auth.service';
import { Observable } from 'rxjs';
import { take, tap, map } from 'rxjs/operators';
import { SettingService } from '../Services/setting.service';

@Injectable({
  providedIn: 'root'
})
export class GuardPage implements CanActivate {

  constructor(
    private router: Router,
    private zone: NgZone,
    private authService: AuthService,
    private settingService: SettingService
  ) { }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isAuth
      .pipe(
        take(1),
        map(result => {
          if(result === false) {
            this.zone.run(() => this.router.navigate(['singIn']));
            return false;
          }

          if (!this.settingService.settingIsExist()) {
            this.zone.run(() => this.router.navigate(['setting']));
            return false;
          }

          return true;
        })
      )
  }
}

