import { Injectable, NgZone } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './../Services/auth.service';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GuardSignIn implements CanActivate {
  constructor(
    private router: Router,
    private zone: NgZone,
    private authService: AuthService
  ) { }
  
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isAuth
      .pipe(
        tap((result) => {
          if (result === true) {
            this.zone.run(() => {
              this.router.navigate(['setting']);
            })
          }
        }),
        map(result => !result)
      )
  }
}
