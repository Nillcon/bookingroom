export enum CurrentRoomState {
  AVAILABLE,
  MEETING_SOON,
  IN_PROGRESS
}

export const EVENT_STATE_TEXT = {
  [CurrentRoomState.AVAILABLE]: {
    statusText: "Avaliable",
    buttonText: "Book now"
  },
  [CurrentRoomState.MEETING_SOON]: {
    statusText: "Meeting starts soon",
    buttonText: "Book later"
  },
  [CurrentRoomState.IN_PROGRESS]: {
    statusText: "Meeting in process",
    buttonText: "Book later"
  }
};