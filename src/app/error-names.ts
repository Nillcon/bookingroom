export enum ErrorName {
    timeRangeEmpty,
    authError,
    dailyLimitExceeded,
    userRateLimitExceeded,
    usageLimits,
    notFound,
    conditionNotMet,
    popup_closed_by_user

}

export const ERROR_MESSAGE: Object = {
    [ErrorName.timeRangeEmpty]: "This can mean that a required field or parameter has not been provided, the value supplied is invalid, or the combination of provided fields is invalid. Check the entered data.",
    [ErrorName.authError]: "Invalid authorization header. The access token you're using is either expired or invalid. Try to re-login",
    [ErrorName.dailyLimitExceeded]: "The Courtesy API limit for your project has been reached. Try coming back a little later",
    [ErrorName.userRateLimitExceeded]: "The per-user limit from the Developer Console has been reached. Try coming back a little later",
    [ErrorName.usageLimits]: "The user has reached Google Calendar API's maximum request rate per calendar or per authenticated user. Try coming back a little later",
    [ErrorName.notFound]: " The specified resource was not found. Try to repeat the request.",
    [ErrorName.conditionNotMet]: "The etag supplied in the If-match header no longer corresponds to the current etag of the resource. Re-fetch the entity and re-apply the changes.",
    [ErrorName.popup_closed_by_user]: "Popup was closed"
}