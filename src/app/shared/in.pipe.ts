import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'in'
})
export class InPipe implements PipeTransform {

  transform(value: any, args?: any): any {
  	if(args.indexOf(value) !== -1) {
  		return true
  	}
  	return false
  }

}
